'''
Ok here me out: we have 1 target site so far so we're just going to yolo this
until we have another piece to add so for now we get a poc working then we clean
things up because im done faffing about
'''
from .item import Item
from products import By

from random import randint
from time import sleep
import traceback

from selenium import webdriver
from os import getenv
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By as SelBy
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

Products = [
    Item('kroger-russet-potatoes', 5, By.Pounds, '0001111091760'),
    Item('roma-tomato', 1, By.Count, '0000000004087'),
    Item('large-green-bell-pepper', 1, By.Count, '0000000004065'),
    Item('green-onions', 1, By.Count, '0000000004068'),
    Item('jumbo-red-onions', 1, By.Count, '0000000004082'),
    Item('white-onions', 1, By.Count, '0000000004663'),
    Item('fresh-banana-each', 1, By.Count, '0000000004011'),
    Item('fresh-strawberries', 1, By.Pounds, '0003338320027')
]


def get_browser() -> Firefox:
    options = webdriver.FirefoxOptions()
    options.add_argument('--headless')
    options.binary_location = '/snap/firefox/current/firefox.launcher'
    browser = webdriver.Firefox(options=options)
    return browser

def wait():
    '''
    Waiting some random amount of time before firing again to avoid an accidental
    rate limiting issue
    Higher limits when on gitlab since we have cloud IP's
    '''
    env = getenv('CI')
    lower, upper = (10, 30) if env is not None else (5, 10)
    # Wait up to 30 seconds before the page to load
    time = randint(lower, upper)
    print(f'Sleeping for {time} seconds... CI = {env}')
    sleep(time)

def price_from_page(browser: Firefox, link: str) -> str:
    browser.get(link)
    WebDriverWait(browser, 30).until(
        EC.presence_of_element_located((SelBy.CLASS_NAME, 'kds-Price-superscript' ))
    )
    elem = browser.find_element(SelBy.CLASS_NAME, 'kds-Price-promotional')
    return str(elem.text).replace('\n', '').replace(' ', '')

def run_kroger_fetch() -> [Item]:
    browser = get_browser()
    ret = []
    for p in Products:
        try:
            print('Fetching ', p)
            # WARN: for now we're just keeping things as str's
            p.price = price_from_page(browser, p.link)
            ret.append(p)
        except Exception:
            print('[ERROR] Skipping', p)
            traceback.print_exc()
        finally:
            wait()
    return ret

    
