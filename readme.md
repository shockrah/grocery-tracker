[![pipeline status](https://gitlab.com/shockrah/grocery-tracker/badges/main/pipeline.svg)](https://gitlab.com/shockrah/grocery-tracker/-/commits/main)


# Grocer Data

This project aims to provide some tooling for fetching, storing and analyzing
data from public Grocers that are based in the US in order to track prices
of various products over time.


## What data/products do we aim to track

Right now we aim to target staple foods as they provide a good baseline for
what North Americans eat over other food products. Keep in mind that 
we primarily refer to "products" as "stuff you buy at the grocery store" which
are commonly used as ingredients. A best effort to keep "compound" foods out of
the list unless they are seen as reasonably common ( such as cereals ). _In this
case "compound foods" are products that contain many ingredients_.

* Breads -> Rye/Wheat/White/Sourdough

* Meats -> Pork/Beef/Chicken

Boneless and "bone-in" products will be counted seperate.

* Starches/Tubers -> Variety of potatoes, corn, and sugars as well

## Workflows

To make this happen we have 3 main workflows (see folders for more info):

1. Data Fetching -> `fetcher/`

2. Data Storage -> `storage/`

3. Analysis -> `analyzer/` & `notebooks/`

_Tooling information can be found in each folder_.

## References

1. https://education.nationalgeographic.org/resource/food-staple/

2. https://environmentamerica.weebly.com/staple-foods.html


hi



