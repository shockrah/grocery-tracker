import json
from kroger import run_kroger_fetch
from argparse import ArgumentParser

if __name__ == '__main__':
    parser = ArgumentParser(
        description='Main development script for testing purposes'
    )


    parser.add_argument('-g', '--grocers', action='append', nargs='*')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument(
        '-o',
        '--outfile',
        nargs=1,
        type=str
    )

    args = parser.parse_args()
    json_file = 'data.json' if args.outfile is None else args.outfile

    # Grab some args
    if args.grocers is not None:
        [ print (i) for i in args.grocers ]

    # Checking for env file first then we load env vars after as kind of 
    # override to whatever is in there
    try:
        data = {
            'kroger': [a.json() for a in run_kroger_fetch()]
        }
        with open(json_file, 'w') as outfile:
            json.dump(data, outfile)
    except KeyboardInterrupt:
        print('Manual interruption detected exiting now')
        exit(0)
