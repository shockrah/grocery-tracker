# NOTE: In case we need to to re-export stuff from sub-mods we can do this
# from . import mod
# from .mod import *

from random import seed, randint
from typing import Optional
import json

def as_json(f):
    def to_json(*args, **kwargs):
        return json.dumps(f(*args, **kwargs))
    return to_json

class Product:
    '''
    Represents a product 
    '''
    def __init__(self, **kwargs):
        # init seed for prng
        seed()
        # Using 8 byte maxes for the sake of simplicity, and to reduce any chance
        # of id collision
        self.id: int = randint(0x0000000000000000, 0xffffffffffffffff)
        self.name: str = kwargs['name']
        self.vendor: str = kwargs['vendor']
        # USD for our purposes
        self.price: float = kwargs['price']
        # Date this record was tracked
        self.date: float = kwargs['date']
        # No decision on how to format this one  yet
        self.location: Optional[str] = kwargs['loc']

    @as_json
    def data(self):
        return self.__dict__()


class ProductCollection:
    def __init__(self, products: list(Product), date: str):
        self.items = products
        self.date = date
        self.vendors = {prod.vendor for prod in products}

    @property
    def size(self):
        return len(self.items)

    @as_json
    def data(self):
        return self.__dict__()




