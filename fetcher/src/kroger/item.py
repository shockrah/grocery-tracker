from products import ApiProductSchema

class Item(ApiProductSchema):
    def __init__(self, name: str, amt: int, by: str, upc: str):
        super().__init__(name, amt, by, upc)
        # Define our own base URL
        self.baseUrl = f'https://www.kroger.com/p/{name}/{upc}'

