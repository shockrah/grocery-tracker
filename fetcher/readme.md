# Tooling


In general most grocer's out there do not have API's so in order to fetch
data we'll have to scrape it off of they're website. Code for scrapers can
be found in the `scrapers/` directory while any sites that have an API
that we can leverage will likely be in the `apis/` directory.

### Linting

> `ruff`

A quick `ruff .` in the virtual environment is best for checking linting errors
as the rules are defined in `pyproject.toml`.

## Scrapers

Scrapers for the most part use `python3.10+` and try to avoid using features
that pin the python version. Backend tooling includes a healthy mix of:

* requests
* Beautiful Soup
* Selenium ( where required ) - _I tend to stray away from this one because it makes developing headless ci/cd pipelines a nightmare but sometimes it's just unavoidable_

## Products that we fetch


| Brand |     Product     | Amount | Unit Type |
|-------|-----------------|--------|-----------|
| Kroger | Russet Potato| | 5      |  Pounds   |
| Kroger | Roma Tomato    | 1      |  Count    |
| Kroger | Green Bell Ppr | 1      |  Count    |
| Kroger | Green Onion    | 1      |  Count    |
| Kroger | Red Onion      | 1      |  Count    |
| Kroger | White Onion    | 1      |  Count    |



## Logging

`-v` or `--verbose` can be used to enable more verbose logging.


## API Reference

_Note: we are using selenium due to Kroger's API being near unusable_

Kroger: https://developer.kroger.com/reference#section/Introduction
