from enum import Enum

class SchemaError(Exception):
    pass

class By(str, Enum):
    Pounds = 'lb'
    Kilos  = 'kg'
    Volume = 'vl'
    Liters = 'l'
    Count  = 'ct'

class ApiProductSchema:
    def __init__(self, name: str, amt: int, by: str, upc: str):
        self.name   = name
        self.amount = amt
        self.by = by
        self.upc = upc
        self.baseUrl: None|str = None
        self.price: None|float|str = None
        if self.by not in By:
            raise SchemaError(f"Bad 'by' value -> {by}")

    @property
    def link(self) -> str:
        return f'{self.baseUrl}/{self.upc}'

    def __str__(self) -> str:
        return f"{self.name} | {self.amount} | {self.by} | {self.link}"

    def json(self) -> dict:
        return self.__dict__


